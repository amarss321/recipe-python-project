variable "prefix" {
  default = "amar"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
}